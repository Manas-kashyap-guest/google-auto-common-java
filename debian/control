Source: google-auto-common-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Olek Wojnar <olek@debian.org>,
Build-Depends: debhelper-compat (= 13),
               default-jdk,
               libguava-java,
               libjavapoet-java,
               maven-debian-helper,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/java-team/google-auto-common-java.git
Vcs-Browser: https://salsa.debian.org/java-team/google-auto-common-java
Homepage: https://github.com/google/auto

Package: libgoogle-auto-common-java
Architecture: all
Depends: ${misc:Depends}, ${maven:Depends}
Suggests: ${maven:OptionalDepends}
Description: Set of common utilities to help ease use of annotation processing
 Java is full of code that is mechanical, repetitive, typically untested, and
 sometimes the source of subtle bugs. Sounds like a job for robots! The Auto
 subprojects are a collection of code generators that automate those types of
 tasks. They create the code you would have written, but without the bugs. Save
 time. Save code. Save sanity.
 .
 This package contains the common utilities.
